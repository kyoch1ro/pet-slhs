<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>

<div class="d-flex">
  <div class="sidebar-holder">
    <!-- REQUIRE YOUR SIDEBAR HERE -->
    <?php require 'our-school/sidebar.php'  ?>
  </div>
  <div class="p-1 w-100">
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-Taekwondo.jpg" data-slb-active="1" data-slb-asset="1874058447" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="Taekwondo International Meet" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-Taekwondo.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-Edufighter-120516.jpg" data-slb-active="1" data-slb-asset="1465701786" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="MathScore National Competition 2016" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-Edufighter-120516.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-eScientist-120516.jpg" data-slb-active="1" data-slb-asset="206316695" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="1st eScience National Olympiad" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-eScientist-120516.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-111016.jpg" data-slb-active="1" data-slb-asset="460624911" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="Provincial Athletic Meet 2016" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-111016.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees07.jpg" data-slb-active="1" data-slb-asset="1208534186" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="Provincial Athletic Meet 2016" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees07.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-092216-00.jpg" data-slb-active="1" data-slb-asset="1530779692" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-092216-00.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-092216-03.jpg" data-slb-active="1" data-slb-asset="1611878182" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-092216-03.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-092216-02.jpg" data-slb-active="1" data-slb-asset="162034053" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees-092216-02.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees_092216-01.jpg" data-slb-active="1" data-slb-asset="1237132728" data-slb-internal="0" data-slb-group="51"><img width="593" height="424" class="aligncenter wp-image-1694 " alt="" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees_092216-01.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees_092216-01.jpg 800w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees_092216-01-300x214.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2017/04/Awardees_092216-01-768x548.jpg 768w" sizes="(max-width: 593px) 100vw, 593px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-MathScore.jpg" data-slb-active="1" data-slb-asset="51230823" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1674" alt="MathScore National Competition" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-MathScore-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-MathScore-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-MathScore-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-MathScore.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-GSP-Chief.jpg" data-slb-active="1" data-slb-asset="2075501675" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1667" alt="2015 Chief Girl Scout of the Philippines" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-GSP-Chief-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-GSP-Chief-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-GSP-Chief-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-GSP-Chief.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-NAPSPHIL.jpg" data-slb-active="1" data-slb-asset="1654056274" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1664" alt="" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-NAPSPHIL-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-NAPSPHIL-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-NAPSPHIL-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-NAPSPHIL.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-LAPRISADA-HS-Acad.jpg" data-slb-active="1" data-slb-asset="734166475" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1663" alt="2015 LAPRISADA High School Academic Competition" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-LAPRISADA-HS-Acad-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-LAPRISADA-HS-Acad-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-LAPRISADA-HS-Acad-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-LAPRISADA-HS-Acad.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Basketball.jpg" data-slb-active="1" data-slb-asset="1702809591" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1671" alt="Provincial Basketball Elementary" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Basketball-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Basketball-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Basketball-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Basketball.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Raqueteers.jpg" data-slb-active="1" data-slb-asset="2136035097" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1675" alt="Provincial Badminton and Lawn Tennis" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Raqueteers-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Raqueteers-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Raqueteers-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Raqueteers.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Taekwondo.jpg" data-slb-active="1" data-slb-asset="771314626" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1672" alt="DepEd Provincial Taekwondo" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Taekwondo-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Taekwondo-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Taekwondo-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Taekwondo.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Swimming.jpg" data-slb-active="1" data-slb-asset="1878288696" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1673" alt="DepEd Provincial Swimming" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Swimming-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Swimming-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Swimming-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Provincial-Swimming.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-Elem.jpg" data-slb-active="1" data-slb-asset="1877870974" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1665" alt="2015 SPPRISAA Elementary Academic Competition" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-Elem-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-Elem-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-Elem-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-Elem.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-HS.jpg" data-slb-active="1" data-slb-asset="1219899593" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1666" alt="2015 SPPRISAA High School Academic Competition" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-HS-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-HS-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-HS-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-SPPRISAA-HS.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Pasiklaban.jpg" data-slb-active="1" data-slb-asset="1353889517" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1676" alt="2015 Pasiklaban sa Paaralan" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Pasiklaban-300x200.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Pasiklaban-300x200.jpg 300w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Pasiklaban-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2016/01/2015-Pasiklaban.jpg 1215w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Division-Press.jpg" data-slb-active="1" data-slb-asset="2048937089" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1621" alt="2015 LT Tennis" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Division-Press-1024x683.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Unit-Basketball.jpg" data-slb-active="1" data-slb-asset="2058703292" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1621" alt="2015 LT Tennis" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Unit-Basketball-1024x683.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Unit-Swimming.jpg" data-slb-active="1" data-slb-asset="1770865022" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1621" alt="2015 LT Tennis" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Unit-Swimming-1024x683.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Unit-Taekwondo.jpg" data-slb-active="1" data-slb-asset="1817385438" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1621" alt="2015 LT Tennis" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Unit-Taekwondo-1024x683.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Badminton-Unit.jpg" data-slb-active="1" data-slb-asset="1896144803" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1621" alt="2015 LT Tennis" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Badminton-Unit-1024x683.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-LT-Tennis.jpg" data-slb-active="1" data-slb-asset="1677810790" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1621" alt="2015 LT Tennis" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-LT-Tennis-1024x683.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-LT-Tennis-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-LT-Tennis-300x200.jpg 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-District-Basketball.jpg" data-slb-active="1" data-slb-asset="1427687080" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1621" alt="2015 LT Tennis" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-District-Basketball-1024x683.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-NAPSPHIL-Speech-Choir.jpg" data-slb-active="1" data-slb-asset="1674793001" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1622" alt="2015 NAPSPHIL Speech Choir" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-NAPSPHIL-Speech-Choir-1024x683.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-NAPSPHIL-Speech-Choir-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-NAPSPHIL-Speech-Choir-300x200.jpg 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DESPC.jpg" data-slb-active="1" data-slb-asset="560752989" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1620" alt="2015 DSSPC Filipino." src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DESPC-1024x683.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DESPC-Collab.jpg" data-slb-active="1" data-slb-asset="603630713" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1620" alt="2015 DSSPC Filipino." src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DESPC-Collab-1024x683.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DSSPC-Filipino..jpg" data-slb-active="1" data-slb-asset="1060807519" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1620" alt="2015 DSSPC Filipino." src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DSSPC-Filipino.-1024x683.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DSSPC-Filipino.-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DSSPC-Filipino.-300x200.jpg 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DSSPC-English.jpg" data-slb-active="1" data-slb-asset="1708148956" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1619" alt="2015 DSSPC English" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DSSPC-English-1024x683.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DSSPC-English-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-DSSPC-English-300x200.jpg 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Sayawit-HS.jpg" data-slb-active="1" data-slb-asset="492296896" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter wp-image-1623" alt="2015 Sayawit Elem" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Sayawit-Elem-1024x683.jpg" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Sayawit-Elem-1024x683.jpg 1024w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2015/10/2015-Sayawit-Elem-300x200.jpg 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/City-Jr-Officers.png" data-slb-active="1" data-slb-asset="1197506807" data-slb-internal="0" data-slb-group="51"><img width="600" height="360" class="aligncenter size-full wp-image-1397" alt="City Jr Officers" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/City-Jr-Officers.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/City-Jr-Officers.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/City-Jr-Officers-300x180.png 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Letterlinx.png" data-slb-active="1" data-slb-asset="1909289241" data-slb-internal="0" data-slb-group="51"><img width="600" height="900" class="aligncenter size-full wp-image-1393" alt="Letterlinx" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Letterlinx.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Letterlinx.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Letterlinx-200x300.png 200w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Interschool-MCL-Cosplay.png" data-slb-active="1" data-slb-asset="979976687" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter size-full wp-image-1389" alt="Interschool - MCL Cosplay" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Interschool-MCL-Cosplay.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Interschool-MCL-Cosplay.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Interschool-MCL-Cosplay-300x200.png 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/MCL-Band.png" data-slb-active="1" data-slb-asset="1668851562" data-slb-internal="0" data-slb-group="51"><img width="600" height="900" class="aligncenter size-full wp-image-1395" alt="MCL Band" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/MCL-Band.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/MCL-Band.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/MCL-Band-200x300.png 200w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/MathScore.png" data-slb-active="1" data-slb-asset="935956383" data-slb-internal="0" data-slb-group="51"><img width="600" height="900" class="aligncenter size-full wp-image-1394" alt="MathScore" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/MathScore.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/MathScore.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/MathScore-200x300.png 200w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Provincial-Meet.png" data-slb-active="1" data-slb-asset="221640712" data-slb-internal="0" data-slb-group="51"><img width="600" height="900" class="aligncenter size-full wp-image-1396" alt="Provincial Meet" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Provincial-Meet.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Provincial-Meet.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/Provincial-Meet-200x300.png 200w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DLSU-Eco-Dance-6x4.png" data-slb-active="1" data-slb-asset="453298033" data-slb-internal="0" data-slb-group="51"><img width="600" height="400" class="aligncenter size-full wp-image-1388" alt="DLSU Eco-Dance 6x4" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DLSU-Eco-Dance-6x4.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DLSU-Eco-Dance-6x4.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DLSU-Eco-Dance-6x4-300x200.png 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DSSPC-English-small.png" data-slb-active="1" data-slb-asset="1314718895" data-slb-internal="0" data-slb-group="51"><img width="600" height="900" class="aligncenter size-full wp-image-1391" alt="DSSPC English small" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DSSPC-English-small.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DSSPC-English-small.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DSSPC-English-small-200x300.png 200w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DSSPC-Filipino.png" data-slb-active="1" data-slb-asset="1447583919" data-slb-internal="0" data-slb-group="51"><img width="600" height="900" class="aligncenter size-full wp-image-1392" alt="DSSPC Filipino" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DSSPC-Filipino.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DSSPC-Filipino.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/DSSPC-Filipino-200x300.png 200w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/District-Meet.png" data-slb-active="1" data-slb-asset="490944298" data-slb-internal="0" data-slb-group="51"><img width="600" height="360" class="aligncenter size-full wp-image-1398" alt="District Meet" src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/District-Meet.png" srcset="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/District-Meet.png 600w, http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/12/District-Meet-300x180.png 300w" sizes="(max-width: 600px) 100vw, 600px"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/images/achievements/ach43.jpg" data-slb-active="1" data-slb-asset="1578207212" data-slb-internal="0" data-slb-group="51"><img width="600" height="800" class="aligncenter" alt="Our School Achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach43.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/images/achievements/ach42.jpg" data-slb-active="1" data-slb-asset="893949541" data-slb-internal="0" data-slb-group="51"><img width="600" height="800" class="aligncenter" alt="Our School Achievement" src="http://sanlorenzoschool.edu.ph/images/achievements/ach42.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/images/achievements/ach40.jpg" data-slb-active="1" data-slb-asset="1668233378" data-slb-internal="0" data-slb-group="51"><img width="600" height="429" class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach40.jpg"></a></p>
        <hr>
        <p><a href="http://sanlorenzoschool.edu.ph/images/achievements/ach38.jpg" data-slb-active="1" data-slb-asset="653073466" data-slb-internal="0" data-slb-group="51"><img width="600" height="800" alt="" src="http://sanlorenzoschool.edu.ph/images/achievements/ach38.jpg"></a></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach37.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach36.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach35.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach34.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach33.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach31.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach30.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach29.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach28.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach27.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach26.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach25.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach24.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach23.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach22.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach21.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach20.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach19.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach18.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach17.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach16.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach15.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach14.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach13.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach12.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach11.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach10.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach9.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach8.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach7.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach6.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach5.jpg"></p>
        <hr>
        <p><img class="aligncenter" alt="achievements" src="http://sanlorenzoschool.edu.ph/images/achievements/ach4.jpg"></p>
    </div>
  </div>
</div>
</body>
</html>