<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>

<div class="d-flex">
  <div class="sidebar-holder">
    <!-- REQUIRE YOUR SIDEBAR HERE -->
    <?php require 'our-school/sidebar.php'  ?>
  </div>
  <div class="p-1 w-100">
    <h3>VISION</h3>
    <p><strong>&nbsp;</strong>SAN LORENZO SCHOOL envisions itself as an agent of change and as a producer of productive citizens of this country who in the near future will actively participate in the economic prosperity, national development, and social transformation through excellence in teaching and community involvement.</p>
    <h3>MISSION</h3>
    <p>SAN LORENZO SCHOOL, although a non-sectarian school, fosters a Christian Community whose members are strongly committed in their covenant with GOD by emulating SAN LORENZO RUIZ’ steadfast faith and unwavering devotion to God, and by advocating Christian Values.</p>
    <p>The school enhances academic excellence in every San Lorenzonian by establishing relevant, responsive, and technologically-oriented academic and non-academic programs; and by continuously upgrading competent and committed teachers whose dedication in their academic profession is beyond compare.</p>
  </div>
</div>
</body>
</html>