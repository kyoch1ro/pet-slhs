<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>
<?php ensureIsLoggedIn() ?>


<?php   
$message = '';
$default_date = dateToLocal();
if(isset($_POST['submit'])) {
    $conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);
    $sql = '';
    if ($_POST['id'] > 0) { // UPDATE
        $sql = "UPDATE events 
                SET 
                title='". $_POST['title'] ."', 
                content='". $_POST['content'] ."',
                end_at='". $_POST['end_at'] ."',
                start_at='". $_POST['start_at'] ."'
                WHERE id = '". $_POST['id'] ."'";
    }else { //INSERT
        $sql = "INSERT INTO events (title, content, end_at, start_at ) 
                VALUES ('". $_POST['title']."','". $_POST['content'] ."','". $_POST['end_at'] ."','". $_POST['start_at'] ."')";
    }
    if(!mysqli_query($conn, $sql)) {
        $message = "<span class='text-danger'>Error: " . $sql . "<br>" . mysqli_error($conn) . "</span> <br>";
    }else {
        $message = "<span class='text-success'> Data was successfully submitted </span> <br>";
    }
    mysqli_close($conn);
}
?>

<?php
if (isset($_GET['view'])) {
    $conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);
    $sql = "SELECT * FROM events WHERE id = " . $_GET['view'];
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $id = $row['id']; 
        $title = $row['title']; 
        $content = $row['content']; 
        $start_at = dateToLocal($row['start_at']); 
        $end_at = dateToLocal($row['end_at']); 
    }else {
        header("Location: /slhsweb/");
    }
}
?>

<div class="d-flex">
    <div class="sidebar-holder">
        <?php require_once 'static_files/sidebar.php'  ?>
    </div>
    <div class="p-1 ">
        <h2 class="reset"><?php echo isset($_GET['view']) ? 'Update' : 'Create' ?> Event</h2>
        <form action="" method="post">
            <input type="hidden" name="id" value="<?php echo $id ?? 0?>">
            <div class="d-flex flex-column mb-1">
                <label for="">Title</label>
                <input name="title" value="<?php echo $title ?? '' ?>" class="styled" type="text" required>
            </div>
            <div class="d-flex flex-column">
                <label for="">Content</label>
                <textarea name="content" id="" cols="30" rows="10" class="styled" required><?php echo $content ?? '' ?></textarea>
            </div>

            <div class="d-flex">
                <div class="d-flex flex-column">
                    <label for="">Start at</label>
                    <input name="start_at" value="<?php echo $start_at ?? $default_date ?>" class="styled" type="datetime-local" required>
                </div>
                <div class="d-flex flex-column">
                    <label for="">End At</label>
                    <input name="end_at" value="<?php echo $end_at ?? $default_date ?>" class="styled" type="datetime-local" required>
                </div>
            </div>
            <br>
            <div><?php echo $message ?></div>
            <button name="submit" type="submit" class="btn">Submit</button>
        </form>
    </div>
</div>
</body>
</html>


