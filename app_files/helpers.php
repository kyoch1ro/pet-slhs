<?php



function viewDate($date) {
    $dt=date_create($date);
    return date_format($dt,"D F j, Y");
}

function truncate($text, $chars = 40){
    if (strlen($text) <= $chars) {
        return $text;
    }
    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    $text = $text."...";
    return $text;

}


function dateToLocal($args = null) {
    if ($args) return date('Y-m-d\TH:i', strtotime($args));
    return date('Y-m-d\TH:i');
   
}



function isLoggedIn() {
    return isset($_SESSION['USER_ID']);
}

function signout() {
    session_destroy();
    header("Location: /slhsweb/");
}

function ensureIsLoggedIn() {
    if (!isLoggedIn()) header("Location: /slhsweb/login.php");
}


function login($username, $password) {
    $conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);
    $sql = "SELECT * FROM accounts WHERE username = '$username'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        if (!password_verify($password, $row['password'])) return false;
        $_SESSION['USER_ID'] =  $row['id'];
        mysqli_close($conn);
        return true;
    } else {
        mysqli_close($conn);
        return false;
    }
    
}