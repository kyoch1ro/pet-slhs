<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>

<div class="d-flex">
  <div class="sidebar-holder">
    <!-- REQUIRE YOUR SIDEBAR HERE -->
    <?php require 'our-program/sidebar.php'  ?>
  </div>
  <div class="p-1 w-100">
        <h3>GUIDELINES FOR ADMISSION AND ENROLLMENT</h3>
        <h3>A.&nbsp; AGE REQUIREMENTS</h3>
        <p>1.&nbsp;&nbsp; <em>Nursery</em>:&nbsp;&nbsp; 3 to 3½ years old.</p>
        <p>2.&nbsp; <em>Junior Kindergarten</em>:&nbsp;&nbsp; 4 to 5 years old who are observed to be capable of self-expression and care.</p>
        <p>3.&nbsp;&nbsp; <em>Senior Kindergarten</em> (Advanced Casa): 5 to 5½ years old are given an entrance examination for proper placement.</p>
        <p>If the applicant failed to meet the set standards but:</p>
        <ol type="a">
        <li>the child is within the age limit 5 to 5½ by June of the current year, and</li>
        <li>the child has the potential for the Senior Kinder level,&nbsp; the child shall be placed in the Junior Kindergarten until he/she is evaluated to be capable of tackling Senior Kindergarten work.</li>
        </ol>
        <p><strong>NOTE:</strong>&nbsp;&nbsp; Rudimentary reading, mathematical skill, independence at work, and over-all maturity are required in a child to be admitted to this level.</p>
        <p>4.&nbsp;&nbsp; <em>Grade 1</em>:&nbsp; 6 to 7 years old.</p>
        <p>&nbsp;</p>
        <h3>B.&nbsp; OTHER REQUIREMENTS TO BE SUBMITTED TO THE REGISTRAR’S OFFICE BEFORE THE ENTRANCE EXAMINATION OR INTERVIEW OF NEW STUDENTS/PUPILS:</h3>
        <p><strong>1.&nbsp;&nbsp; PRE-ELEMENTARY LEVEL:</strong></p>
        <ol type="a">
        <li>photocopy of birth certificate</li>
        <li>photocopy of baptismal certificate&nbsp; (for Catholic applicants only)</li>
        <li>photocopy of child’s health/immunization record</li>
        <li>photocopy of report card/Form 138&nbsp; (for Senior Kinder applicants only)</li>
        </ol>
        <p><strong>2.&nbsp;&nbsp; GRADE SCHOOL LEVEL:</strong></p>
        <ol type="a">
        <li>photocopy of birth certificate</li>
        <li>photocopy of baptismal certificate&nbsp; (for Grades 1 to 4 Catholic applicants)</li>
        <li>photocopy of report card/Form 138 from previous school</li>
        <li>certificate of good moral character</li>
        <li>certificate of scholastic standing/rank from the principal of the previous school (for honor students)</li>
        </ol>
        <p><strong>NOTE:</strong>&nbsp; Only those applicants who passed the entrance exam (grade placement exam) are accepted in the Grade School.&nbsp; The grade placement exam taken and passed should conform with the grade eligibility of the child in his/her report card/F-138.</p>
        <p><strong>3.&nbsp;&nbsp; HIGH SCHOOL LEVEL:</strong></p>
        <ol type="a">
        <li>general average of not less than 83%</li>
        <li>report card from previous school/Form 138</li>
        <li>letter of recommendation or certificate of good moral character from the principal/guidance counselor of the former school</li>
        <li>medical certificate</li>
        <li>photocopy of birth certificate</li>
        <li>certificate of scholastic standing/rank from the principal of the previous school (for honor students)</li>
        <li>S<strong><em>cience Curriculum</em></strong><em>:</em> Freshmen applicants may be admitted to the Science Curriculum, if, in addition to the above requirements, the applicant:</li>
        <li style="list-style-type: none;">
        <ol type="1">
        <li>has final grades not lower than 85% in all grade six subjects and a general average of 85% and above;</li>
        <li>with an “above average” score in the entrance exam;</li>
        <li>with “above average” marks in the M.A.T. exams in Mathematics, Science and English.</li>
        </ol>
        </li>
        </ol>
        <p>&nbsp;</p>
        <h3>C.&nbsp; ADMISSION PROCEDURE</h3>
        <p>1. Secure and fill up an <a href="http://sanlorenzoschool.edu.ph/admission-form/">admission form</a>. <a href="http://sanlorenzoschool.edu.ph/pdf/application-admission-form.pdf" target="_blank">(or download our form)</a></p>
        <p>2. Submit the required documents to the Registrar’s Office.</p>
        <p>3. Schedule the appointment for an interview or assessment.</p>
        <p>4. Show up at the appointed day and time.</p>
        <p>5. Pursue the Guidance and Testing officer’s recommendation.</p>
        <p><strong><em>NOTE:</em></strong><em>Only those applicants who passed the entrance exam and the interview conducted by the Guidance Staff will be eligible to enroll.</em></p>
        <p><a href="http://sanlorenzoschool.edu.ph/admission-form/">FILL UP OUR ADMISSION FORM</a> | <a href="http://sanlorenzoschool.edu.ph/pdf/application-admission-form.pdf" target="_blank">DOWNLOAD OUR FORM</a></p>
  </div>
</div>
</body>
</html>