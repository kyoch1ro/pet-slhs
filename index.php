<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>
<img src="Banner.jpg" style="width:1247px;height:350px;">
<br>

<div class="d-flex flex-column">
    <div class="d-flex">
        <div class="margin-sm padding-sm this-is-inline has-border">
            <center><h2> Mission </h2></center>
            SAN LORENZO SCHOOL although a non-sectarian school, fosters a Christian Community whose members are strongly committed in their covenant with GOD by emulating SAN LORENZO RUIZ, steadfast faith and unwavering devotion to God, and by advocating Christian Values.
            </div>
        <div class="margin-sm padding-sm this-is-inline has-border2">
            <center><h2> Vision </h2></center>
            SAN LORENZO SCHOOL envisions itself as an agent of change and as a producer of productive citizens of this country who in the near future will actively participate in the economic prosperity, national development, and social transformation through excellence in teaching and community involvement.
        </div>
    </div>
    <div class="d-flex">
        <div class="margin-sm padding-sm this-is-inline has-border news">
            <center><h2>NEWS</h2></center>
            <?php  require_once 'dynamic_files/news/list.php'; ?>
        </div>
        <div class="margin-sm padding-sm this-is-inline has-border2 events">
            <center><h2>Events</h2></center>
            <div class="bg-white p-1">
                <?php  require_once 'dynamic_files/events/calendar.php'; ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>