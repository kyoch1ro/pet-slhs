<?php require_once 'app_files/init.php'  ?>
<?php if(isLoggedIn()) header("Location: /slhsweb/") ?>



<?php
if(isset($_POST['username']) && isset($_POST['password'])) {
    if (login($_POST['username'], $_POST['password'])) {
        header("Location: /slhsweb");
    }else {
        $message = 'Incorrect username or password';
    }
}
?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>





<div class="d-flex content-holder ">
    <div class="m-auto">
        <div class="bg-white p-1 round-corner">
            <h2>Welcome to San Lorenzo Web Portal</h2>
            <h3>Please log in to your account</h3>
            <?php if (isset($message)) echo '<p class="text-danger">'.$message.'</p>' ?>
            <form action="login.php" method="post">
                <div class="d-flex flex-column mb-1">
                    <label for="">Username</label>
                    <input name="username" class="styled" type="text">
                </div>
                <div class="d-flex flex-column">
                    <label for="">Password</label>
                    <input name="password" class="styled" type="password">
                </div>
                <button name="login" type="submit" class="btn">Submit</button>
            </form>
        </div>
                
    </div>
</div>

</body>
</html>