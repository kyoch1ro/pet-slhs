<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>

<div class="d-flex">
  <div class="sidebar-holder">
    <!-- REQUIRE YOUR SIDEBAR HERE -->
    <?php require 'our-school/sidebar.php'  ?>
  </div>
  <div class="p-1 w-100">
    
  <table border="0" width="100%">
    <tbody>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img class="aligncenter" src="http://sanlorenzoschool.edu.ph/images/prepre.jpg" alt="" width="300" height="200"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE PRE–ELEMENTARY BUILDING</h3>
    <p>Constructed in 2000 on the corner of Maharlika Drive and Malaya St., a few meters away from the main campus, the well-secured 3-storey building houses the Nursery, Junior, and Senior Kinder classrooms on the first floor, the Grade One classrooms on the second floor, and the play area on the third floor. This building has its own Computer Laboratory for the Junior Kinder to Grade One pupils, Audio Visual Room, Pre-Elementary Faculty Lounge, Pre-Elementary Mini Library, and other offices. Each fully air-conditioned room in this building is equipped with Montessori materials, 21st century educational tools, and its own lavatory &amp; rest room to allow better monitoring of every pupil in the class and provide convenience to the users.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/images/mainmain.jpg" alt="" width="300" height="200"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE MAIN CAMPUS</h3>
    <p>The main campus located at 608 Mayumi St., is a well secured sprawling compound that houses interconnected structures which include the Administrative Offices, Elementary Building, Central Annex Building, High School Building, School Gymnasium, School Chapel, HE/TLE Casita, and Cafeteria. This campus has classrooms and laboratories fitted with 21st century learning tools that enable it to be transformed into internet laboratories. The campus ground is a designated WiFi Zone where students can connect to the internet with their wireless devices for research purposes. The airy campus surrounded lightly with trees, shrubs, and flowering plants has enough ground to serve as a venue for several types of activities and accommodate a large assembly of students from the pre-school to the secondary level.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-achievements.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE ADMINISTRATION BUILDING</h3>
    <p>The oldest structure in this compound which used to serve as the Abanilla family’s residence now houses the offices of the Directors, Principal, Registrar, and Accounting as well as the Piano Room, Conference Room, and Faculty Lounge. The Administration Building showcases various achievements of San Lorenzo School throughout the years with its display of trophies, plaques, and awards from different academic and non-academic competitions held in the area, district, regional, and national level.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-elementary.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE ELEMENTARY BUILDING</h3>
    <p>The 3-storey structure has 12 classrooms for the Grades 2 to 6 students in the Elementary department. The lower floor is used by the Primary level while the upper floors are used by the Intermediate level. Each classroom is spacious enough to accommodate a maximum of 35 students. This building has a library exclusively for the use of elementary students.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-hs.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE HIGH SCHOOL BUILDING</h3>
    <p>This 4–storey structure which started as a 2-storey building is occupied by students in the High School level from Grade 7 to Grade 10. Among the 21 rooms in this building are the Applied Science Laboratory, Bio-Chemistry Laboratory, air-conditioned Audio Visual Rooms, Soft Trades Laboratory, Hard Trades Laboratory, Printing Room, Students Assembly and Journalism Office, and the Office of the Facility Development Supervisor.</p>
    <p>&nbsp;</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-entrance.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE CENTRAL ANNEX BUILDING</h3>
    <p>This 3-storey structure has transformed into an annex of the Elementary, High School, and Administration buildings in the Main Campus. The ground floor accommodates the School Clinic, the Guidance Office, and the Speech Laboratory. The second floor shares the High School Library, Elementary Computer Laboratory, and the High School Computer Laboratory. The third floor has 3 rooms that are used as additional classrooms for High School and Elementary levels.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-gym.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE GYMNASIUM</h3>
    <p>The covered gymnasium is a multi-function, well equipped facility used for different events such as basketball, volleyball, badminton, stage presentations, assemblies, and other school activities that help enhance the social, cultural, physical, and health development of every student. This structure is fitted with public address speakers, an audio booth, and modern lighting equipments. The basketball court has a FIBA standard design to ensure that the students are properly trained in the sport according to official standards.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-cafeteria.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE CAFETERIA</h3>
    <p>This is a place reserved exclusively for dining with designated dining places for the students and personnel of the school. Delicious and nutritious food and refreshments can be purchased at affordable prices from various concessionaires who are strictly required to practice proper hygiene and sanitation in food serving or preparation.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE GUIDANCE OFFICE</h3>
    <p>An extension of the administrative office, the Guidance Office located on the ground floor of The Central Annex Building is always ready to provide counseling services and orientation on school policies and regulations with the help of its competent staff composed of Psychometricians, the Prefect of Discipline, and the Guidance Counselor. This office is also equipped with updated testing materials that could further enhance the abilities and thinking skills of students from all levels.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE SCHOOL CLINIC</h3>
    <p>The School Clinic has the basic equipments and medical supplies needed for first–aid treatments in cases of medical ailment, injury, or emergency while in school. With a school nurse on duty 5 days a week and immediate access to the school doctor or school dentist, students and personnel are assured of proper medical or dental attention when the need arises.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-speechlab.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE SPEECH LABORATORY</h3>
    <p>A fully air-conditioned and well–maintained laboratory used to enhance and continuously upgrade the oral communication skills of students in all levels. This facility is equipped with individual audio booths which can accommodate up to a maximum of 40 students.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-cafeteria2.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>RETAIL STORE</h3>
    <p>Adjacent to the Cafeteria and the High School building, the store offers school supplies, uniforms, and other materials that are often used by students in their day to day learning. The Retail Store also provides the High School students the hands-on experience in retailing and application of some of the business concepts.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-casita.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE TECHNOLOGY AND LIVELIHOOD EDUCATION (TLE) CASITA</h3>
    <p>The TLE Casita is a homely place adjacent to the Cafeteria where students learn the lessons on technology and livelihood in a true household setting, complete with the main rooms of a household like the living room, dining, and kitchen area where students can cook up all–time favorites or new recipes.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-pclab.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE COMPUTER LABORATORIES</h3>
    <p>Located on the 2nd floor of the Annex, the fully air-conditioned Computer Laboratories, one for the Elementary and another for the High School Department, are well-equipped with updated network of computers and software programs to introduce basic and advanced Information Technology concepts and applications. Through wireless connectivity, students, faculty, and personnel are kept updated with the developments in the fast-paced world of the internet and high–tech gadgets and new information. The laboratories are furnished with tables with customized consoles for the computer hardware to allow students’ unrestricted view of the board during lectures and plenty of desk space for writing. Each laboratory can accommodate a maximum of 36 students to ensure a 1 computer per student ratio.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-hslibrary.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE HIGH SCHOOL LIBRARY</h3>
    <p>This air-conditioned place is a learning resource center for the exclusive use of High School students to help broaden their knowledge through the available literary resources and internet access that it offers. There is a periodic acquisition of reference books and related materials like encyclopedias, atlas, almanac, news magazines, and others to further equip students with valuable and updated information.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE ELEMENTARY LIBRARY</h3>
    <p>It is an air-conditioned room containing reference books and reading materials relevant for the elementary students’ research activities and assignments. Certain days are allotted for reading exploration under the English subject wherein students may borrow books of their preference to take home in order to reinforce their appreciation toward reading as a hobby.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"><img src="http://sanlorenzoschool.edu.ph/wp-content/uploads/2014/09/pic-chapel1.jpg" alt=""></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE SCHOOL CHAPEL</h3>
    <p>A place of worship for the members of the San Lorenzo School family, the fully air-conditioned Chapel was constructed in 2007 to provide Christian students a center for religious activities such as recollections, thanksgiving masses, and Bible studies. This facility is the last known legacy of the foundress, Mrs. Zenaida A. Abanilla.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE SCIENCE ROOM</h3>
    <p>This room adjacent to the Elementary Building is equipped with visual aids, Science related materials and resources, and multimedia equipments to enhance the learning experience of Elementary students on the subject matter.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE C.A.T. HEADQUARTERS</h3>
    <p>The Citizen’s Army Training Headquarters is located behind the Elementary building. It serves as the office of the CAT Commandant and CAT officers of San Lorenzo School. It also provides storage for CAT and athletic equipments.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE APPLIED SCIENCE LABORATORY</h3>
    <p>Located on the 2nd floor of the High School building, the Applied Science Laboratory is used for lectures and hands-on laboratory experience to aid the students understanding of the laws of Physics and its relevance in the real world. This room is furnished with multimedia equipments, laboratory materials, and functional laboratory tables for the learning convenience of the students.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE BIO-CHEMISTRY LABORATORY</h3>
    <p>Located beside the Physics Laboratory, the Bio-Chemistry Lab is filled with laboratory materials, tiered tables, and multimedia equipments used for visual demonstrations, actual observations, and hands–on experimentations to facilitate a deeper conception of the subject matter, as well as ignite the students’ curiosity to search for answers to questions that often lead to useful discoveries.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE SOFT TRADES LABORATORY</h3>
    <p>Located on the ground floor of the High School Building, this recent addition allows female high school students to learn soft trades such as dressmaking and hair and nail care as part of their Technology and Livelihood Education elective.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE HARD TRADES LABORATORY</h3>
    <p>Located on the ground floor of the High School Building, this recent addition allows male high school students to learn different hard trades such as Electrical, Electronics, PC Assembly and Troubleshooting, and Drafting as part of their Technology and Livelihood Education elective.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE MOBILE COMPUTER LABORATORY</h3>
    <p>With the use of 21st century learning tools such as computer tablets, laptops, and/or smart internet televisions that can be moved from one room to the other, any classroom in the Main Campus and the Pre-Elementary Building can easily be transformed into a computer laboratory where learning can be enhanced with the aid of supplemental computer-aided interactive or online programs.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE SLS PARENT TEACHER ASSOCIATION OFFICE</h3>
    <p>A latest addition to accommodate the needs of Parents and Teachers, the SLS-PTA Office located in the ground floor of the Pre-Elementary Building is the designated headquarters of the Parent-Teacher officers of San Lorenzo School in order to better work hand–in–hand with the school in the realization of the various projects and outreach programs of the San Lorenzo School Parent–Teachers Association.</p></td>
    </tr>
    <tr>
    <td style="vertical-align: top; padding: 20px;" width="40%"></td>
    <td style="vertical-align: top; padding: 10px;" width="60%">
    <h3>THE LOCKER AREA</h3>
    <p>Located on the ground floor of the High School Building, this recent addition allows male high school students to learn different hard trades such as Electrical, Electronics, PC Assembly and Troubleshooting, and Drafting as part of their Technology and Livelihood Education elective.</p></td>
    </tr>
    </tbody>
    </table>

  </div>
</div>
</body>
</html>