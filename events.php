<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<?php $button = (isLoggedIn()) ? '<a href="event-form.php" class="btn">Add</a>' : ''; ?>

<body>
<?php require_once 'dynamic_files/navbar.php'  ?>
<div class="d-flex">
  <div class="sidebar-holder">
    <?php require_once 'static_files/sidebar.php'  ?>
  </div>

    <div class="p-1 w-100">
    <?php 
        if(isset($_GET['view'])) {
            require_once 'dynamic_files/events/byid.php';
        }else {
          echo '
              <div class="d-flex justify-content-between align-content-start">
                <h1 class="reset">San Lorenzo Events</h1>
                '. $button .'                  
              </div>
              <br>';
            require_once 'dynamic_files/events/list.php';

        }
    ?>
  </div>
</div>
</body>
</html>