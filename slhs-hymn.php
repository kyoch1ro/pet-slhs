<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>

<div class="d-flex">
  <div class="sidebar-holder">
    <!-- REQUIRE YOUR SIDEBAR HERE -->
    <?php require 'our-school/sidebar.php'  ?>
  </div>
  <div class="p-1 w-100">
        <p align="center"><strong>SAN LORENZO</strong><strong> SCHOOL HYMN</strong></p>
        <p align="center">With every step we take,<br>
        With every ground we break,<br>
        That we may err<br>
        We shall not fear –<br>
        Forward with uplifted hearts.<br>
        Rise up to greater heights!<br>
        Hail! San Lorenzo our<br>
        Alma mater dear!</p>
        <p align="center">The sweet and gentle guidance<br>
        Of our mentors dear,<br>
        The comforting love and assurance<br>
        Of our parents near,<br>
        We’ll always remember.<br>
        Hail! San Lorenzo our<br>
        Alma mater dear!</p>
        <p align="center">The glory of our school<br>
        On our success will grow.<br>
        Wherever we may go,<br>
        Whomsoever may call,<br>
        We shall not fear,<br>
        Hail! San Lorenzo<br>
        We’ll always be true to you.</p>
        <p align="center">Hail! San Lorenzo  forever!</p>
        <p align="center"><em>Lyrics by:  Mrs. Linda Santos</em><br>
        <em>Music by: Mr.  Sin Elman</em><br>
        <em>Arranged by:  Prof. Emmanuel Laureola</em></p>
  </div>
</div>
</body>
</html>