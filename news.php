<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>

<?php

$button = (isLoggedIn()) ? '<a href="news-form.php" class="btn">Add</a>' : '';

?>
<div class="d-flex">
  <div class="sidebar-holder">
    <?php require_once 'static_files/sidebar.php'  ?>
  </div>

    <div class="p-1 w-100">
    <?php 
        if(isset($_GET['view'])) {
            require_once 'dynamic_files/news/byid.php';
        }else {
          echo '
          <div class="d-flex justify-content-between">
            <h1 class="reset">San Lorenzo News</h1>
            '. $button .'                  
          </div>
          <br>
            ';
            require_once 'dynamic_files/news/list.php';
        }
    ?>
  </div>
</div>
</body>
</html>


