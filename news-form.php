<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>
<?php ensureIsLoggedIn() ?>


<?php   
$message = '';
if(isset($_POST['submit'])) {
    $conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);
    $sql = '';
    if ($_POST['id'] > 0) { // UPDATE
        $sql = "UPDATE news 
                SET title='". $_POST['title'] ."', 
                content='". $_POST['content'] ."'
                WHERE id = '". $_POST['id'] ."'";
    }else { //INSERT
        $sql = "INSERT INTO news (title, content) 
                VALUES ('". $_POST['title']."','". $_POST['content'] ."')";
    }
    if(!mysqli_query($conn, $sql)) {
        $message = "<span class='text-danger'>Error: " . $sql . "<br>" . mysqli_error($conn) . "</span> <br>";
    }else {
        $message = "<span class='text-success'> Data was successfully submitted </span> <br>";
    }
    mysqli_close($conn);
}
?>

<?php
if (isset($_GET['view'])) {
    $conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);
    $sql = "SELECT * FROM news WHERE id = " . $_GET['view'];
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        $id = $row['id']; 
        $title = $row['title']; 
        $content = $row['content']; 
    }else {
        header("Location: /slhsweb/");
    }
}
?>

<div class="d-flex">
    <div class="sidebar-holder">
        <?php require_once 'static_files/sidebar.php'  ?>
    </div>

    <div class="p-1 ">
        <h2 class="reset"><?php echo isset($_GET['view']) ? 'Update' : 'Create' ?> News</h2>
        <form action="" method="post">
            <input type="hidden" name="id" value="<?php echo $id ?? 0?>">
            <div class="d-flex flex-column mb-1">
                <label for="">Title</label>
                <input name="title" value="<?php echo $title ?? '' ?>" class="styled" type="text" required>
            </div>
            <div class="d-flex flex-column">
                <label for="">Content</label>
                <textarea name="content" id="" cols="30" rows="10" class="styled" required><?php echo $content ?? '' ?></textarea>
            </div>
            <br>
            <div><?php echo $message ?></div>
            <button name="submit" type="submit" class="btn">Submit</button>
        </form>
    </div>
</div>
</body>
</html>


