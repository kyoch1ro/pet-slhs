<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>

<div class="d-flex">
  <div class="sidebar-holder">
    <!-- REQUIRE YOUR SIDEBAR HERE -->
    <?php require 'our-school/sidebar.php'  ?>
  </div>
  <div class="p-1 w-100">    
    <h3 align="center"><strong>SCHOOL PHILOSOPHY AND GOALS</strong></h3>
    <p>San Lorenzo School, a reputable center for youth education, strongly believes that quality education is extremely essential to the growth of the nation and its people. Though this school is co-educational and non-sectarian, it provides teaching about Christian Living to students regardless of their religious affiliation.</p>
    <p>San Lorenzo School offers well-balanced curricula and well-planned academic program in pre-elementary, elementary, and high school levels and well-planned academic program to upgrade the quality of the school’s instruction in all subject areas. Its academic strength is maintained by highly competent teachers and administrators, effective instructional materials, rigid academic supervision, and better facilities.</p>
    <p>Through the years, the school has proven to the community that it is a helpful partner to the parents in developing their children. It has consistently maintained and inculcated its primary aim – the promotion and furtherance of the cause of their children to assist them in harnessing their full potentials to become the best persons that they can be as creative social agents in the building of a better civilization.</p>
    <p>This aim is based on San Lorenzo School’s belief in the innate goodness of the individual because of Christ’s life in him. Given the necessary trust and nurturing for independence, consistent training and guidance in Christian moral values, and a prepared environment, the child will spontaneously constitute into a person who will be a&nbsp; “citizen of the world”; a “complete human being able to exercise in freedom, a self-disciplined will and judgment unperverted by prejudice and undistorted by fear.”</p>
    <p>Moreover, the secondary level is envisioned as an Entrepreneurial Development Institution that would seek to develop the student’s skills in starting a business and likewise to expose and familiarize them in the world of work.</p>
    <p>Finally, San Lorenzo School aims to help our people in realizing that we are all spiritually united in building a better world and that we plan to accomplish this through the child whom we train and mold to be the hope and agent of positive change in the future.</p>
  </div>
</div>
</body>
</html>