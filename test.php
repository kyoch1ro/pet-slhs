<?php require_once 'app_files/init.php'  ?>
<?php require_once 'dynamic_files/header.php'  ?>
<body>
<?php require_once 'dynamic_files/navbar.php'  ?>

<div class="d-flex">
  <div class="sidebar-holder">
    <!-- REQUIRE YOUR SIDEBAR HERE -->
    <?php require 'our-school/sidebar.php'  ?>
  </div>
  <div class="p-1 w-100">
    <!-- PUT OR REQUIRE YOUR CONTENT HERE -->
    <?php require 'our-school/coontent.php'  ?>
  </div>
</div>
</body>
</html>