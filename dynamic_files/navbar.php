<a href="login.php"><img src="Headerlogo.png"></img></a>
<ul class="d-flex">
  <li><a href="index.php">Home</a></li>
  <li class="dropdown">
    <button class="dropbtn">Our School 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <?php require 'our-school/sidebar.php'; ?>
    </div>
  </li>

  <li class="dropdown">
    <button class="dropbtn">Our Program
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <?php require 'our-program/sidebar.php'; ?>
    </div>
  </li>

  <li><a href="Ourpeople.html">Our People</a></li>
  <li><a href="Ourgallery.html">Our Gallery</a></li>
  <li class="ml-auto">
    <?php 
      if(isLoggedIn()) {
        echo '<a href="signout.php">Sign out</a>';
      }else {
        echo '<a href="login.php">Sign in</a>';
      }
    
    ?>
    
  </li>
</ul>