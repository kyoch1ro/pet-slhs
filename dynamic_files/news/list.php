<?php 
    $conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);

    $sql = "SELECT * FROM news";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "<div class='mb-1'>
                        <h2 class='reset'><a href='news.php?view=".$row['id']."'>". $row["title"] ."</a> <br> <small>". viewDate($row['created_at'])."</small></h2>
                        <p class='reset'>". truncate($row["content"], 80) ."</p>
                    </div>";
        }
    } else {
        echo "There no news as of this moment";
    }
    mysqli_close($conn);
?>