<?php 
    $conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);
    $sql = "SELECT * FROM events";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "<div classs='mb-1'>
                        <h2 class='reset'>
                            <a href='events.php?view=".$row['id']."'>". $row["title"] ."</a>
                            <br> 
                            <small>Event date: ". viewDate($row['start_at']). " to ".  viewDate($row['end_at']) ."</small>
                        </h2>
                        <p class='reset'>". truncate($row["content"], 80) ."</p>
                    </div>";
        }
    } else {
        echo "There no events as of this moment";
    }
    mysqli_close($conn);
?>