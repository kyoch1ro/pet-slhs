<?php 
    $conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);
    $sql = "SELECT * FROM events WHERE id = " . $_GET['view'];
    $result = mysqli_query($conn, $sql);
    $button = (isLoggedIn()) ? "
        <div class='d-flex'>
            <a href='event-form.php?view=".$_GET['view']."' class='btn mr-1'>Edit</a>
            <a href='event-delete.php?view=".$_GET['view']."' class='btn danger'>Delete</a>
        </div>" : '';

    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "  <div>
                        <div class='d-flex justify-content-between align-content-start '>
                            <h1 class='reset'>
                                ". $row["title"] ."
                                <br> 
                                <small>Event date: ". viewDate($row['start_at']). " to ".  viewDate($row['end_at']) ."</small> 
                            </h1>
                            ". $button ."
                        </div>
                        <br>
                        <p class='reset'>". $row["content"]."</p>
                        
                    </div>
                    <br>
                    <br>
                    <a href='events.php'>Back to list</a>";
        }
    } else {
        header("Location: /slhsweb/");
    }
    mysqli_close($conn);
