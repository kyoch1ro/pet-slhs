<div id="full-calendar"></div>

<?php 
$conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS, DB_NAME);

$sql = "SELECT * FROM events";
$result = mysqli_query($conn, $sql);



?>
<script>
    $(document).ready(function() {
    $('#full-calendar').fullCalendar({
        height: 600,
        events: [

            <?php 
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while($row = mysqli_fetch_assoc($result)) {
                        echo "{

                                title: '". $row['title'] ."',
                                start: '". dateToLocal($row['start_at']) . "',
                                end: '". dateToLocal($row['end_at']) . "',
                                url: 'events.php?view=". $row['id'] ."'
                               },";
                    }
                }
                mysqli_close($conn);
            ?>
        ]
    })
    });
</script>