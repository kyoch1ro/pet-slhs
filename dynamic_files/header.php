<html>
<head>
<link rel="stylesheet" type="text/css" href="navbar.css">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="vendor/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet" href="style.css">


<script src="vendor/jquery/jquery-3.1.1.min.js"></script>
<script src='vendor/fullcalendar/lib/moment.min.js'></script>
<script src='vendor/fullcalendar/fullcalendar.min.js'></script>
<script src='custom.js'></script>

</head>